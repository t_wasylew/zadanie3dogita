import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Write gender");
        try {
            String whatGender = scanner.nextLine();
            System.out.println("Wpisano " + Gender.valueOf(whatGender.toUpperCase().trim()));
        } catch (IllegalArgumentException iae) {
            System.out.println(iae.getMessage());
        }
    }
}
